# 项目介绍
> 一个提供API接口供开发者调用的平台
- 这里是列表文本管理员可以接入并发布接口，统计分析个接口的调用情况
- 用户可以注册登录并开通接口调用权限
- 浏览接口及在线调试，还能使用客户端SDK轻松在代码中调用
# 业务流程
![业务流程图](image/%E4%B8%9A%E5%8A%A1%E6%B5%81%E7%A8%8B%E5%9B%BE.png)
# 技术选型
## 前端
- Ant Design Pro
- React
- Ant Design Procomponents
- Umi
- Umi Request (Axios的封装)
## 后端
- Spring Boot
- Spring Boot Starter (SDK开发)
- Dubbo(RPC)
- Nacos
- Spring Cloud Gateway(网关，限流，日志实现)
# 页面展示
## 前端页面
### 主页展示
![主页](image/主页.png)
### 接口管理
![接口管理](image/接口管理.png)
### 查看接口文档
![查看接口文档](image/查看接口文档.png)
### 接口测试
![接口测试](image/)
## 后端swagger页面
![swagger页面](image/swagger.png)