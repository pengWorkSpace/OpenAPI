package com.peng.pengapiinterface.controller;


import com.peng.pengapiclientsdk.model.User;
import com.peng.pengapiclientsdk.utils.SignUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: 小彭
 * @CreateTime: 2023-04-03  15:22
 */
@RestController
@RequestMapping("/name")
public class NameController {

    @GetMapping("/get")
    public String getNameByGet(String name){
        return "get 你的名字是" + name ;
    }

    @PostMapping("/post")
    public String getNameByPost(@RequestParam String name){
        return "POST 用户的名字是" + name ;
    }

    @PostMapping("/user")
    public String getNameByPost(@RequestBody User user, HttpServletRequest request){

        String accessKey = request.getHeader("accessKey");
        String nonce = request.getHeader("nonce");
        String timestamp = request.getHeader("timestamp");
        String sign = request.getHeader("sign");
        String body = request.getHeader("body");

        //todo 实际情况是去数据库中查询是否已分配给用户
        if (!accessKey.equals("peng")){
            throw new RuntimeException("无权限");
        }
        //todo 随机数校验，时间戳校验
        if (Long.parseLong(nonce) > 10000){
            throw new RuntimeException("无权限");
        }
        //todo 实际下面的secretKey要从数据库中查出来
        String genSign = SignUtils.genSign(body, "abcdefgh");
        if (!sign.equals(genSign)){
            throw new RuntimeException("无权限");
        }
        String result =  "POST 用户名字是" + user.getUsername();
        //调用成功后次数加一

        return result;
    }
}
