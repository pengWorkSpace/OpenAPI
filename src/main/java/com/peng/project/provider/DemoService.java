package com.peng.project.provider;

import java.util.concurrent.CompletableFuture;

/**
 * @Author: 小彭
 * @CreateTime: 2023-04-19  14:33
 */
public interface DemoService {

    String sayHello(String name);

    String sayHello2(String name);

    default CompletableFuture<String> sayHelloAsync(String name) {
        return CompletableFuture.completedFuture(sayHello(name));
    }

}
