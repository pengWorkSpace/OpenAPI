package com.peng.project.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.peng.common.model.entity.UserInterfaceInfo;

import java.util.List;


/**
* @author 13905
* @description 针对表【user_interface_info(用户调用接口关系)】的数据库操作Mapper
* @createDate 2023-04-09 20:37:29
* @Entity generator.domain.UserInterfaceInfo
*/
public interface UserInterfaceInfoMapper extends BaseMapper<UserInterfaceInfo> {

    List<UserInterfaceInfo> listTopInvokeInterfaceInfo(int i);
}




