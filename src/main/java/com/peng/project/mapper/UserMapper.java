package com.peng.project.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.peng.common.model.entity.User;

/**
 * @Entity com.peng.project.model.domain.User
 */
public interface UserMapper extends BaseMapper<User> {

}




