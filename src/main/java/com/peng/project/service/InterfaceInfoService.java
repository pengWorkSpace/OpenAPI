package com.peng.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.peng.common.model.entity.InterfaceInfo;

/**
* @author 13905
* @description 针对表【interface_info(接口信息)】的数据库操作Service
* @createDate 2023-03-23 17:18:24
*/
public interface InterfaceInfoService extends IService<InterfaceInfo> {

    void validInterfaceInfo(InterfaceInfo interfaceInfo, boolean b);
}
