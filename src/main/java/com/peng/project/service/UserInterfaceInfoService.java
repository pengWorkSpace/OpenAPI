package com.peng.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.peng.common.model.entity.UserInterfaceInfo;


/**
* @author 13905
* @description 针对表【user_interface_info(用户调用接口关系)】的数据库操作Service
* @createDate 2023-04-09 20:37:29
*/
public interface UserInterfaceInfoService extends IService<UserInterfaceInfo> {

    void validUserInterfaceInfo(UserInterfaceInfo userInterfaceInfo, boolean b);

    boolean invokeCount(long interfaceInfoId,long userTd);
}
