package com.peng.pengapiclientsdk;

import com.peng.pengapiclientsdk.client.PengApiClient;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @Author: 小彭
 * @CreateTime: 2023-04-03  22:48
 */
@Configuration
@ConfigurationProperties("pengapi.client")
@Data
@ComponentScan
public class PengApiClientConfig {

    private String accessKey;

    private String secretKey;

    @Bean
    public PengApiClient pengApiClient(){
        return new PengApiClient(accessKey,secretKey);
    }
}
