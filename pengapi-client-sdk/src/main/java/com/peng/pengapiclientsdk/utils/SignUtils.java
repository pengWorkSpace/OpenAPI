package com.peng.pengapiclientsdk.utils;

import cn.hutool.crypto.digest.DigestAlgorithm;
import cn.hutool.crypto.digest.Digester;

/**
 * 生成签名工具类
 * @Author: 小彭
 * @CreateTime: 2023-04-03  17:57
 */
public class SignUtils {
    /**
     * 生成签名
     * @param body
     * @param secretKey
     * @return
     */
    public static String genSign(String body, String secretKey) {
        Digester md5 = new Digester(DigestAlgorithm.SHA256);
// 5393554e94bf0eb6436f240a4fd71282
        String content = body.toString() + "." + secretKey;
        return md5.digestHex(content);
    }
}
