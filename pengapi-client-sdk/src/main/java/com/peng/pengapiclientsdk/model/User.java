package com.peng.pengapiclientsdk.model;

import lombok.Data;

/**
 * @Author: 小彭
 * @CreateTime: 2023-04-03  15:26
 */
@Data
public class User {

    private String username;
}
